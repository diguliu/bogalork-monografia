require 'core/policy'
require 'helpers/math_helper'
require 'games/tic_tac_toe/reward_functions/final_state'

class Policies::TemporalDifference < Policy
  include MathHelper

  attr_accessor :memory, :td_value, :discount, :action_save_knowledge

  def load_defaults
    self.memory ||= [] 
    self.td_value ||= 0
    self.discount ||= 0.8
    self.action_save_knowledge ||= false
    self.reward_function =
      Games::TicTacToe::RewardFunctions::FinalState.new({})
  end

  def decide(state, options)
    Interface.instance.display_policy_start_decide("TD(#{td_value})", state)
    current_reward = nil
    current_option = nil
    options.shuffle!
    if rand < random_chance
      option = options[0]
      reward = state_value(option)
      Interface.instance.display_policy_random_decision(option, reward)
      return option
    end
    options.each do |option|
      reward = state_value(option)
      Interface.instance.display_policy_option(option, reward)
      if !current_reward || reward > current_reward
        current_reward = reward
        current_option = option
      end
    end
    Interface.instance.display_policy_decision(current_option, 
                                               current_reward)
    current_option
  end

  def state_value(state)
    return reward_function.reward(player, state) if !knowledge[state.id]
    if estimation == 'mean'
      mean(knowledge[state.id].map {|key, value| value} <<
           reward_function.reward(player, state))
    elsif estimation == 'sum'
      sum(knowledge[state.id].map {|key, value| value}) +
      reward_function.reward(player, state)
    end
  end

  def update_knowledge
    first = self.memory.delete_at(0)
    last = self.memory.pop
    Interface.instance.display_td_start_update(first[:state],
                                               knowledge[first[:state].id],
                                               state_value(first[:state]))

    i=0
    expected_reward = memory.inject(0) { |result, action| i+=1 ; result +=
      (discount**i)*action[:reward] }
    i=1 if i<1
    expected_reward += (discount**i)*state_value(last[:state])
    self.knowledge[first[:state].id] ||= {}
    self.knowledge[first[:state].id][last[:state].id] = expected_reward

    self.memory << last
    Interface.instance.display_td_end_update(knowledge[first[:state].id],
                                             state_value(first[:state]))
  end

  def analyze_step(player, state)
    if memory.size >= td_value+2
      update_knowledge
    end
    self.memory << {:state => state, 
                    :reward => reward_function.reward(player,state)}
    Interface.instance.display_td_add_new_state(memory)
  end

  def analyze_round(round)
    while(self.memory.size > 1)
      update_knowledge
    end
    self.memory.clear
=begin
    require 'ap'
    if !round[:winner].nil? && round[:winner] != player
      round[:state_sequence].map { |state| puts state.to_s }
    end
=end
  end
end
