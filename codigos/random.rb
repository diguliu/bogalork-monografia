require 'core/policy'

class Policies::Default < Policy
  def decide(state, options)
    Interface.instance.display_random_policy_decide('==> Random decision')
    options[rand(options.size)]
  end
end

