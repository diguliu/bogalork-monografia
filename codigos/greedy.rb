require 'core/policy'

class Policies::Greedy < Policy
  def decide(state, options)
    Interface.instance.display_policy_start_decide('Greedy', state)
    current_reward = nil
    current_option = nil
    # Just to avoid local cliffs
    options.shuffle!
    options.each do |option|
      reward = knowledge[option.id] || reward_function.reward(player, option)
      Interface.instance.display_policy_option(option, reward)
      if !current_reward || reward > current_reward
        current_reward = reward
        current_option = option
      end
    end
    Interface.instance.display_policy_decision(current_option, current_reward)
    current_option
  end
end
