require 'games/tic_tac_toe/reward_functions'
require 'core/reward_function'

class Games::TicTacToe::RewardFunctions::FinalState < RewardFunction
  def reward(player, state)
    result = state.check
    if result.has_key?(:winner)
      value = result[:winner] == player.symbol ? reward_value : reward_value*-1
    else
      value = 0
    end
    Interface.instance.display_final_state_reward(state, value)
    return value
  end

  def reward_value
    10
  end
end

