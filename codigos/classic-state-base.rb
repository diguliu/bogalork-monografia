require 'core/state'
require 'games/tic_tac_toe/states'

class Games::TicTacToe::States::ClassicState < State
  # Available attributes:
  #   + player
  #   + board

  # Function: defines the initial state of the board
  def initial_board
  end

  # Function: format the board in a displayable way
  # Return: String
  def to_s
  end

  # Function: calculate all reachable states from this state
  # Return: Array<State>
  # Annotations: you might use the method #clone_board if you want to have an
  # independent copy of the board
  def next_possible_states
  end

  # Function: evaluate the situation of the state
  # Return: Hash = {:final => true/false, :winner => <if-any-winner>}
  def check
  end
end
