SOURCEDOCUMENT=monografia
PDFVIEWER=evince
CAPITULOS=$(wildcard *.tex)

#######################################

all: $(SOURCEDOCUMENT).pdf

view: $(SOURCEDOCUMENT).pdf
	$(PDFVIEWER) $(SOURCEDOCUMENT).pdf &

$(SOURCEDOCUMENT).pdf: $(SOURCEDOCUMENT).tex $(CAPITULOS) $(SOURCEDOCUMENT).bib
	pdflatex -shell-escape $< && \
	makeindex -s tabela-simbolos.ist -o $(SOURCEDOCUMENT).sigla $(SOURCEDOCUMENT).siglax && \
	bibtex $(SOURCEDOCUMENT) && \
	pdflatex -shell-escape $< && \
	pdflatex -shell-escape $< || \
	$(RM) $@

clean:
	$(RM) *.aux *.bbl *.blg *.lof *.lot *.log $(SOURCEDOCUMENT).pdf *~ \
	*.toc *.ilg *.sigla *.siglax *.symbols *.symbolsx
