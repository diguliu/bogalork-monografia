\chapter{Instanciando o Framework - O Caso do \TicTacToe}
\label{chap:instanciando-o-framework}
A fim de demonstrar o funcionamento do \framework, após a sua construção, foi realizada uma instanciação do \framework para servir como caso de prova. Essa instância tem como objetivo construir \ias para o jogo \TicTacToe. Esse jogo foi escolhido principalmente por ser um jogo resolvido, portanto é possível ter uma prova concreta da eficácia do treinamento realizado no \framework. A seguinte descrição de como essa instanciação foi realizada também servirá como um guia prático de como criar instâncias no \framework.

O \framework vem composto do \script \instantiation, localizado na pasta \scripts, que torna a tarefa de instanciar o \framework muito mais simples e intuitiva. Além disso, o \instantiation provêem uma maneira segura de instanciar o \framework seguindo o padrão adequado, garantindo maior estabilidade e escalabilidade da instalação. De toda forma, mesmo com o \instantiation, é possível realizar a instanciação do \framework manualmente. Neste guia prático iremos mostrar a instanciação do \framework para o jogo \TicTacToe através do \instantiation. Todos os comandos executados neste capítulo supõe que o usuário está na raiz do \framework. Para ter uma lista de todos os comandos disponíveis no \script \instantiation basta executar:

\begin{minted}{sh}
  ./script/instantiation
\end{minted}

\section{Instanciando o Jogo}
\label{sec:instanciando-o-jogo}
O primeiro passo para instanciar o \framework é definir o jogo que será instanciado. A partir disso, executamos o seguinte comando:

\begin{minted}{sh}
  ./script/instantiation create game <nome_do_jogo>
\end{minted}

Esse comando irá criar o arquivo \textit{games/<nome\_do\_jogo>.rb} com as definições do jogo e também irá criar a pasta \textit{games/<nome\_do\_jogo>} com as sub-pastas \textit{reward\_functions} e \textit{states} onde deverão ser criadas as \rewardfunctions e os \states, respectivamente, para o jogo. Veremos como instanciar cada um desses elementos nas seções \ref{subsec:instanciando-a-funcao-de-recompensa} e \ref{subsec:instanciando-o-modelo-de-estados}.

O arquivo base com a definição do jogo \TicTacToe criado pelo \script por ser visto no apêndice \ref{subsec:tic-tac-toe-base-code}.

Este arquivo já é criado com algumas informações básicas necessárias para a integração dele com \framework, como por exemplo a herança com a classe \Game e os métodos recomendados para serem estendidos. Além disso, ele também possui uma breve documentação acima explicitando os atributos disponíveis na classe e uma descrição de cada um dos métodos de extensão explicando sua função, seu comportamento padrão, seu tipo de retorno e cada um de seus parâmetros. Este padrão foi adotado para todos os arquivos de base no \framework por permitir que o usuário possa criar a sua instância do \framework sem ter que necessariamente compreender a infra-estrutura do \Core, se preocupando somente com os aspectos relacionados a sua instância.

Para nossa implementação específica do \TicTacToe, decidimos estender apenas o método que define o primeiro jogador, variando este a cada round, e mantivemos o comportamento padrão para a definição do próximo jogador. O arquivo final pode ser visto no apêndice \ref{subsec:tic-tac-toe-code}.

Esse procedimento de instanciação do jogo é, basicamente, o mesmo procedimento para a instanciação de qualquer outro elemento do \framework. Nas próximas seções vamos nos ater simplesmente a demonstrar o que foi feito no estudo de caso do \TicTacToe uma vez que as demais informações genéricas já estão esclarecidas nesta seção.

\subsection{Instanciando o Modelo de Estados}
\label{subsec:instanciando-o-modelo-de-estados}
Para instanciarmos um novo modelo de estados para um jogo devemos executar o seguinte comando:

\begin{minted}{sh}
  ./script/instantiation create state <nome_do_jogo> <nome_do_modelo_de_estados>
\end{minted}

Esse comando irá criar o arquivo \textit{games/<nome\_do\_jogo>/states/<nome\_do\_modelo\_de\_estados>.rb}. O arquivo base com a definição do modelo de estados \ClassicState pode ser visto no apêndice \ref{subsec:classic-state-base-code}. A versão final desse nosso modelo de estados pode ser vista no apêndice \ref{subsec:classic-state-code}.

\subsection{Instanciando a Função de Recompensa}
\label{subsec:instanciando-a-funcao-de-recompensa}
Para instanciarmos uma nova \rewardfunction para um jogo devemos executar o seguinte comando:

\begin{minted}{sh}
  ./script/instantiation create reward_function \
  <nome_do_jogo> <nome_da_funcao_de_recompensa>
\end{minted}

Daí temos \textit{games/<jogo>/reward\_functions/<nome\_da\_funcao\_de\_recompensa>.rb}. O arquivo base com a definição da \rewardfunction \FinalState pode ser visto no apêndice \ref{subsec:final-state-base-code}.

Ao longo da implementação do jogo foram criadas duas \rewardfunctions: a \FinalState, como mostrada acima, e a \AllStates. A \FinalState é uma \rewardfunction sem \ia que simplesmente recompensa um estado de vitória com o valor 10, um estado de derrota com um valor -10 e qualquer outro estado com o valor 0. A \AllStates é uma função com \ia treinada da seguinte forma: sempre que um jogador chegar a um estado de vitória todos os estados que o levaram àquele estado recebem +1 na sua recompensa. Caso o estado final seja de derrota, todos recebem -1. No caso de empate não tem alterações. No capítulo \ref{chap:resultados} é feita uma análise dos resultados de cada uma dessas \rewardfunctions aplicadas a contextos específicos.

A versão final dessas \rewardfunctions podem ser vistas no apêndice \ref{sec:funcoes-de-recompensa-code}.

\section{Instanciando a Política}
\label{sec:instanciando-a-politica}
Para instanciarmos uma nova \policy no \framework devemos executar o seguinte comando:

\begin{minted}{sh}
  ./script/instantiation create policy <nome_da_politica>
\end{minted}

Esse comando irá criar o arquivo \textit{policies/<nome\_da\_politica>.rb}. O arquivo base com a definição da \policy \Greedy pode ser visto no apêndice \ref{subsec:greedy-base-code}.

Ao fim das instanciações realizadas, foram criadas 4 \policies: \Random, \Greedy, \Human e \TemporalDifference. A \policy \Random não possui \ia e decide aleatoriamente entre as opções de jogada. Já a \Greedy, apesar de não ter \ia escolhe sempre o estado com a maior recompensa dada pela \rewardfunction. Caso exista mais de uma opção com a maior \reward, ela escolhe qualquer uma delas. A \policy \Human foi criada para servir de interface para um jogador humano. No momento de decisão, a política abre espaço para interação do usuário para que ele possa escolher que ação tomar. Por fim, temos a \policy \TemporalDifference implementada tal qual explicada na seção \ref{subsubsec:text-temporal-difference}. Ela foi implementada de forma genérica, portanto com a configuração adequada ela pode funcionar como TD(n), para $n \in \mathbb{N}$. 

A versão final dessas \policies podem ser vistas no apêndice \ref{sec:politicas-code}.

\section{Configurando o Treinamento}
\label{sec:configurando-o-treinamento}
Após realizar a instanciação, é a hora de configurar os treinamentos que se deseja realizar. Nos arquivos de configuração estarão especificados todos os detalhes característicos da execução do treinamento. Existem três tipos de arquivos de configuração: configurações do jogo, do treinamento e da \trainingsequence. Para realizar um treinamento, é necessário compor uma configuração de treinamento com uma configuração de jogo, formando assim a configuração completa de um treinamento. Já a configuração de \trainingsequence já inclui em si ambas as configurações. A configuração de \trainingsequence é mais adequada de se utilizar pois ela já faz tudo que a combinação entre a configuração de jogo e treinamento fazem e, além disso, já está integrada com a ferramenta de análise de resultados e geração de gráficos. Ainda assim, falaremos sobre cada uma destas configurações nas próximas seções.

Assim como para a instanciação do \framework nós temos o \script \instantiation, para configurar e executar os treinamentos temos o \script \simulation, localizado também na pasta \scripts. As demonstrações feitas aqui serão utilizando este \script que oferece os mesmos benefícios citados para o \script \instantiation. Para ter uma lista de todos os comandos disponíveis no \script \simulation basta executar:

\begin{minted}{sh}
  ./script/simulation
\end{minted}

\subsection{Jogo}
\label{subsec:configuracoes-do-jogo}
Caso queiramos construir configurações para um jogo, basta executarmos o seguinte comando:

\begin{minted}{sh}
  ./script/simulation create <nome_do_jogo> <nome_da_configuracao>
\end{minted}

Esse comando irá criar o arquivo \textit{configs/games/<nome\_do\_jogo>/<nome\_da\_configuracao>.yml} com um \template básico para as configurações do jogo. Assim como os \templates de instanciação, os \templates de configuração também possuem uma breve descrição de cada configuração. O \template para o arquivo de configuração de jogo pode ser visto no apêndice \ref{subsec:jogo-base-code}.

\subsection{Treinamento}
\label{subsec:configuracoes-do-treinamento}
Para criarmos configurações de um treinamento, basta executarmos o seguinte comando:

\begin{minted}{sh}
  ./script/simulation create training <nome_do_treinamento>
\end{minted}

Esse comando irá criar o arquivo \textit{configs/trainings/<nome\_do\_treinamento>.yml} também com um \template básico. Este \template pode ser visto no apêndice \ref{subsec:treinamento-base-code}. Neste arquivo tem uma configuração que especifica o nome do arquivo de configuração do jogo que deve ser usado no treinamento, desta forma, como poderá ser visto na seção \ref{sec:executando-o-treinamento}, o treinamento já pode ser executado. Como o arquivo de configuração do jogo está independente do de treinamento, você pode ter várias configurações para o seu jogo e executa-los com somente uma configuração de treinamento fazendo essa combinação. Esse é mais um dos mecanismos de modularidade incluídos no \framework com o objetivo de garantir a independência de componentes, inclusive dos arquivos de configuração.

\subsection{Sequências de Treinamento}
\label{subsec:configuracoes-de-sequencias-de-treinamento}
Para criarmos configurações de uma \trainingsequence , basta executarmos o seguinte comando:

\begin{minted}{sh}
  ./script/simulation create sequence <nome_da_sequencia>
\end{minted}

Esse comando irá criar o arquivo \textit{configs/sequences/<nome\_da\_sequencia>.yml} já com um \template básico. A configuração de sequência já inclui a configuração tanto do treinamento quanto do jogo e com isso somente ela já é suficiente para executar a \trainingsequence. O \template base criado pode ser visto no apêndice \ref{subsubsec:sequencia-de-treinamento-base-code}.

Dadas as vantagens de realizar sequências de treinamento ao invés do mero treinamento, todas as configurações criadas para os treinamentos do \TicTacToe foram feitas desta forma. Os treinamentos realizados e o resultado de cada um deles pode ser visto no capítulo \ref{chap:resultados}. Os arquivos de configuração criados para eles podem ser vistos no apêndice \ref{subsec:sequencias-de-treinamento}.

\section{Executando o Treinamento}
\label{sec:executando-o-treinamento}
Uma vez criadas as configurações que especificam o treinamento, agora basta executá-los. Como discutido nas seções anteriores, você pode executar um treinamento independente ou executar uma \trainingsequence. Para ambos temos os seguintes comandos, respectivamente:

\begin{minted}{sh}
  ./script/simulation run training[s] <treinamento1> <treinamento2> ...
\end{minted}
\begin{minted}{sh}
  ./script/simulation run sequence[s] <sequencia1> <sequencia2> ...
\end{minted}

\section{Análise do Treinamento}
\label{sec:analise-do-treinamento}
Após executar a \trainingsequence, o simulador cria um registro completo de tudo que foi feito. Esse registro fica armazenado na pasta \textit{experiments/<nome\_da\_sequencia>}. Como é possível que o usuário queira executar a mesma \trainingsequence novamente sem perder o registro da última execução, o simulador cria pastas em ordem numérica e armazena o registro da nova execução dessa \trainingsequence no próximo número disponível, como pode ser visto na figura \ref{fig:experiments-1st-level}. 

\begin{figure}[H]
  \centering
  \includegraphics[scale=0.8]{imagens/experiments-1st-level.png}
  \caption{Estrutura de diretórios da pasta \textit{experiments/train\_ttt\_td\_mean}}
  \label{fig:experiments-1st-level}
\end{figure}

Esse registro de execução de uma \trainingsequence é composto de três pastas e um arquivo, como pode ser visto na figura \ref{fig:experiments-2nd-level}. 

\begin{figure}[H]
  \centering
  \includegraphics[scale=0.8]{imagens/experiments-2nd-level.png}
  \caption{Estrutura de diretórios da pasta \textit{experiments/train\_ttt\_td\_mean/1}}
  \label{fig:experiments-2nd-level}
\end{figure}

O arquivo \textit{training\_config.yml} é uma cópia das configurações de treinamento usadas naquela \trainingsequence. As pastas são: \textit{policies}, onde estão armazenadas todas as \ias para as \policies treinadas, figura \ref{fig:experiments-policies}, \textit{reward\_functions}, semelhante à \textit{policies}, mas com \rewardfunctions, figura \ref{fig:experiments-reward-functions}, e por fim, a pasta \textit{results}, figura \ref{fig:experiments-results}. 

\begin{figure}[H]
  \centering
  \includegraphics[scale=0.8]{imagens/experiments-policies.png}
  \caption{Estrutura de diretórios da pasta \textit{experiments/train\_ttt\_td\_mean/1/policies}}
  \label{fig:experiments-policies}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[scale=0.8]{imagens/experiments-reward-functions.png}
  \caption{Estrutura de diretórios da pasta \textit{experiments/train\_ttt\_td\_mean/1/reward\_functions}}
  \label{fig:experiments-reward-functions}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[scale=0.8]{imagens/experiments-results.png}
  \caption{Estrutura de diretórios da pasta \textit{experiments/train\_ttt\_td\_mean/1/results}}
  \label{fig:experiments-results}
\end{figure}

Na pasta \textit{results} temos dois arquivos: \textit{final.yml}, que possui a média e o desvio padrão do número de vitórias, empates e derrotas em cada um dos \checkpoints da \trainingsequence, e \textit{final.html} que possui as mesmas informações, porém demonstradas através de um gráfico interativo gerado com a ferramenta \textit{highcharts}\footnote{O \textit{highcharts} é uma biblioteca javascript para geração de gráficos interativos. Mais informações sobre o \textit{highcharts} podem ser encontradas em http://www.highcharts.com/. Último acesso em 11 de Dezembro de 2011.}. Além desses arquivos, existe uma pasta para cada execução do treinamento com os resultados parciais de cada um deles. Através desse registro, toda \trainingsequence executada no \framework fica armazenada com todos os seus detalhes.

