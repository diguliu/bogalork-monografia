\chapter{Resultados}
\label{chap:resultados}
Após a instanciação do \framework para o treinamento de \ias para o jogo \TicTacToe, foram realizados os treinamentos e neste capítulo iremos analisar os resultados desses treinamentos. Ao fim das instanciações, foram construídos três treinamentos distintos, todos com o mesmo objetivo: construir uma \ia que fosse capaz de jogar o jogo \TicTacToe de forma ótima. Todos os treinamentos e o procedimento no qual todos foram baseados serão descritos nas próximas seções.

\section{O Procedimento}
\label{sec:o-procedimento}
O treinamento de \ias é um processo que possui muitas variáveis envolvidas e, portanto, esta sujeito a muitas variações em curva de aprendizado. Por conta disso, se faz necessário definir um procedimento de treinamento e análise dos resultados que leve em conta essas variações e permita uma análise comparativa realista entre as diversas técnicas de aprendizado.

O primeiro ponto a ser considerado é a curva de aprendizado. Cada técnica de aprendizado pode ter uma curva de aprendizado diferente, fazendo com que uma técnica seja melhor que outra até um determinado momento, mas no futuro passe a ser pior. Para analisarmos os comportamento dessa curva, foi criado o conceito de \checkpoint. Um \checkpoint é um marco ao longo do aprendizado onde a \ia é avaliada. Desta forma, dividimos o treinamento em inúmeros \checkpoints, possibilitando a visualização de uma curva bem aproximada do que seria a curva de aprendizado da técnica sendo analisada.

Outro ponto importante é a variação de resultado a cada execução do treinamento. Por ser geralmente uma técnica não-determinística, o aprendizado de máquina tende a ter diferentes resultados a cada execução o que atrapalha uma análise comparativa entre diferentes técnicas. A fim de eliminar essa flutuação de resultados, todos os resultados colhidos foram provenientes de dez execuções do treinamento sobre os quais calculamos a média do resultado de cada \checkpoint e o desvio padrão, para garantirmos que a margem de flutuação estatística fosse baixa.Com esses pontos bem definidos, podemos passar para a definição da avaliação.

Como dito anteriormente, em cada \checkpoint foi realizada uma avaliação do aprendizado. Essa avaliação foi feita usando um jogador aleatório. Em cada um dos \checkpoints, a \ia jogou cem partidas contra um jogador aleatório e foram anotados os números de vitória, derrota e empate. Através desses resultados, limitados pela margem de erro definida com o desvio padrão, pudemos gerar um gráfico para cada técnica de aprendizado utilizada demonstrando sua curva de aprendizado com um bom grau de confiança.

Vamos ver cada um desses treinamentos em detalhes nas próximas seções.

\section{Função de Recompensa \AllStates}
\label{sec:reward-function-all-states}
O primeiro treinamento implementado foi um treinamento de \rewardfunction o qual chamamos de \AllStates. Esse treinamento foi descrito na seção \ref{subsec:instanciando-a-funcao-de-recompensa}. Realizamos o treinamento de acordo com o procedimento especificado anteriormente e obtivemos o resultado demonstrado pela figura \ref{fig:all-states-results}.

\begin{figure}[H]
  \centering
  \includegraphics[width=450px]{imagens/results/all-states.pdf}
  \caption{Resultados do treinamento da \rewardfunction \AllStates}
  \label{fig:all-states-results}
\end{figure}

Os resultados mostram uma curva de aprendizado logarítmica. Esse é um comportamento esperado desse modelo de aprendizado e nós detectamos esse mesmo padrão nos demais treinamentos, com algumas pequenas diferenças. Esse padrão é bastante interessante se tratando de aprendizado pois conseguimos obter rapidamente uma \ia razoável para o jogo. 

O desvio padrão para os resultados apresentados é de aproximadamente 2\%, um valor bastante razoável. A partir disso, podemos ver que a \ia converge para um quadro de somente vitórias e empates, pois embora com 125 mil rounds não tenha sido suficiente para que todas as 10 execuções obtivessem um índice de 0 derrotas, o desvio padrão abrange esse cenário.

\section{\TemporalDifference}
\label{sec:temporal-difference-results}
O segundo e o terceiro treinamento foram focados na \policy usando a técnica \TemporalDifference. Como base para o treinamento da \policy foi criada a \rewardfunction \FinalState descrita na seção \ref{subsec:instanciando-a-funcao-de-recompensa}. Por ser \td, tivemos que definir o valor dela, definindo a janela de distância entre a observação do resultado e a atualização do aprendizado. Definimos ela como TD(0), onde a cada observação de recompensa o estado exatamente anterior tem sua recompensa recalculada. Durante a construção dessa \policy percebemos dois métodos de recalcular esse valor: através da soma ou da média das recompensas dos próximos estados descontados. A partir daí geramos estes treinamentos para avaliarmos qual dos métodos seria mais eficiente para o caso do \TicTacToe. Os resultados desses treinamentos podem ser vistos nas figuras \ref{fig:td-sum-results} e \ref{fig:td-mean-results}.

\begin{figure}[H]
  \centering
  \includegraphics[width=450px]{imagens/results/td-sum-results.pdf}
  \caption{Resultados do treinamento da \policy \TemporalDifference \Sum.}
  \label{fig:td-sum-results}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[width=450px]{imagens/results/td-mean-results.pdf}
  \caption{Resultados do treinamento da \policy \TemporalDifference \Mean.}
  \label{fig:td-mean-results}
\end{figure}

Observando os resultados, percebemos uma clara vantagem da \policy que usa a média em relação a que usa a soma. Este resultado ficou evidente após uma análise mais profunda da execução de cada uma das \policies. A \policy que usa a soma dos próximos valores possui um grande problema. Ao determinar o valor de um estado como a soma dos valores dos próximos, você acaba valorando estados intermediários como mais importantes que estados finais. Utilizando este método, quando mais sub-estados um estado tiver, maior tende a ser sua recompensa. Por conta disso, a \policy sempre tendia a evitar estados finais, muitas vezes adiando a sua vitória e outras deixando de ganhar por conta disso. Ao analisarmos a \policy de média e seus resultados, percebemos que ela era muito mais adequada do que a de soma. Isso porque, ao determinar o valor de um estado como a média dos próximos, o estado finais sempre terão as maiores recompensas (em módulo, considerando os estado de derrota). Esta propriedade é garantida pois existe a taxa de desconto que garante que a influência de um estado futuro é sempre menor quanto mais longe ele estiver. Desta forma, a \policy sempre aproveita a chance de ganhar e sempre evitar um estado que leve a uma derrota subsequente. Não foi a toa que está técnica foi a que obteve menor número de derrotas.

Outro ponto interessante observado dos resultados é um decaimento no número de vitórias em ambos os casos. Estudando o caso da \policy de soma, observamos que essa falha era decorrente justamente do fenômeno descrito acima. Com relação à \policy de média, observamos que o número de vitórias diminuiu, porém o número de derrotas também diminuiu, ambos dando espaço a empates. Esse comportamento indica que a \policy adquiriu uma postura de preferir estados mais seguros que levam a um empate do que arriscar um estado de possível vitória, mas com risco de derrota.

Por fim, foi realizado um teste, apenas por curiosidade e sem rigor estatístico, onde a melhor das \ias construídas através do \TemporalDifference \Mean jogou 3 partidas contra 10 pessoas de níveis diferentes, incluindo o autor, utilizando a \policy \Human. Nenhum humano foi capaz de ganhar uma partida contra esta \ia.
