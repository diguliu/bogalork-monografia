\chapter{Aprendizado de Máquina}
\label{chap:aprendizado-de-maquina}
Aprendizado de máquina é um conceito bastante antigo na área da computação, mas ainda assim continua sendo um conceito bastante estudado por conta da sua complexidade até mesmo em se especificar a sua real definição. Este conceito provem da mesma definição que nós temos de aprendizado, aplicada ao programas de computador. De forma resumida e superficial, aprendizado de máquina é uma área de estudo da inteligência artificial que busca a criação de técnicas de programação que simulem a ideia de aprendizado que nós temos para o computador \cite{mitchell-1997}.

A partir desse conceito, outros diversos conceitos foram se formando. O primeiro e mais básico é o conceito de \textit{agente}. O agente é a entidade computacional que será possuidor do dito "aprendizado". Em consequência da definição de agente, temos o de \textit{\ia}. A \ia é o registro computacional do aprendizado do agente. Tanto o agente como a \ia podem ser definidos computacionalmente das mais diversas formas. Um agente, por exemplo, pode ser um processo em execução ou até mesmo um braço mecânico em uma fábrica. Já uma \ia, pode ser um mero arquivo de log de informações, uma rede neural ou uma complexa estrutura de dados com relacionamentos entre as informações e regras de consistência (como uma ontologia). Não existem limites para esta composição, o importante é que ao juntarmos esses dois conceitos, o agente com uma \ia, temos o alvo do aprendizado de máquina: o agente inteligente.

Ainda assim, somente com esses dois conceitos a definição básica do aprendizado de máquina não está completa. Este agente inteligente precisa de alguma forma obter informações para que ele possa construir a sua \ia e, efetivamente, aprender algo. Daí então nós alcançamos o terceiro pilar do aprendizado de máquina: o \textit{ambiente}. O ambiente é um espaço no qual o agente está inserido ou interage com e de onde o agente obtém as informações necessárias para construir a sua \ia. Esse conceito também é tão genérico quanto os anteriores. Esse ambiente pode ser um conjunto de imagens de entrada que servem de padrão para uma rede neural de reconhecimento ou mesmo o motor de um carro onde um agente obtém leituras da sua temperatura. A questão é que este ambiente pode ser dinâmico e se alterar dadas as \textit{regras} sobre as quais ele está sujeito. É de onde nós temos nosso quarto conceito. As regras definem basicamente como o ambiente se comporta em cada situação. No caso do motor do carro, ele estaria sujeito às regras da termodinâmica que definiriam a sua temperatura.

Com esses quatro conceitos combinados nós temos que pode ser considerado objetivo do aprendizado de máquina definido da seguinte forma: dado um \textit{agente inteligente} (\textit{agente} com uma \textit{\ia}) imerso em um \textit{ambiente} sujeito a determinadas \textit{regras}, o objetivo do aprendizado de máquina é fazer com que esse \textit{agente} construa a sua \textit{\ia} de forma a compreender quais são as \textit{regras} sob as quais o \textit{ambiente} está sujeito.

É com esse objetivo então que surgem as técnicas de aprendizado de máquina. Existem muitas técnicas de aprendizado, cada qual com suas especificidades, vantagens e desvantagens. Na construção desse \framework nos dedicamos ao \ReinforcementLearning, uma técnica de aprendizado baseada na experiência adquirida com a interação do agente com o ambiente. Vamos descrever em maiores detalhes como esse técnica funciona na próxima seção.

\section{Reinforcement Learning}
\label{sec:reinforcement-learning}
O \ReinforcementLearning é um modelo de aprendizado onde o agente, inserido em
um ambiente geralmente desconhecido, realiza ações e, a partir dos resultados
observados no ambiente, readapta sua \policy de escolha de forma a maximizar
suas possibilidades de obter os resultados que ele espera \cite{mitchell-1997}.
Diferentemente do modelo de aprendizado supervisionado onde é "dito"\xspace ao
agente como responder a uma determinada situação, no \ReinforcementLearning o
agente  recebe uma \reward, positiva ou negativa, do ambiente e precisa
aprender, a partir disso, como se comportar no futuro \cite{ghory-2004}. Uma
representação desse modelo pode ser vista na figura \ref{fig:rl-cicle-scheme}.
Apesar de ter do ambiente uma \reward relativa a sua ação, o agente busca,
ainda assim, definir uma \policy que maximize seus resultados a longo prazo e
não somente a curto prazo.

\begin{figure}[H]
  \centering
  \includegraphics[scale=0.5]{imagens/rl-cicle-scheme.png}
  \caption{ciclo de aprendizado no \rl (adaptado de
  \cite{intro-ml-nilsson-1996})}
  \label{fig:rl-cicle-scheme}
\end{figure}

Os principais elementos encontrados em um modelo de \rl são
\cite{intro-ml-nilsson-1996}:
\begin{enumerate}
  \item um conjunto \textit{S} de estados do ambiente;
  \item um conjunto \textit{A} de ações;
  \item uma \rewardfunction \textit{r} para toda ação  $\textit{a} \in$
  \textit{A}; e
  \item uma \policy $\pi$ que define como o agente deve se comportar em um
  estado \textit{S}.
\end{enumerate}

Um dos fatores que torna difícil tecnicamente a aplicação do \rl é que nenhum
dos seus elementos pode ser tratado de forma trivial. Cada um deles possui
características intrínsecas que tornam a composição da solução completa muito
complexa. 
Primeiramente temos a modelagem dos estados do ambiente. Para cada
problema específico temos diversas maneiras de mapeia-los computacionalmente e
a escolha dessa maneira é determinante para o resultado final
\cite{ghory-2004}.
Já o conjunto de ações, possui, na maioria dos problemas, um tamanho inviável
de ser armazenado por completo. Isso limita a solução aos conjuntos que podem
ser representados através de padrões ou fórmulas matemáticas \cite{bouzy}.
Felizmente, no caso dos jogos de tabuleiro, essa é uma característica quase
sempre encontrada.
A \rewardfunction é, na grande maioria dos casos, uma aproximação, uma vez que
está não pode ser uma tabela que mapeie estado-valor dada a limitação espacial.
Portando, esta precisa ser determinada por uma pessoa (um \expert), por uma
função linear, que tende a ser estável e convergente porém pouco expressiva, ou
por um esquema não-linear como redes neurais, que são mais expressivas porém
tendem a não ser estáveis ou convergentes \cite{unifying-rl-2009}. Em qualquer
um dos casos, não se tem um função de \reward evidente, acrescentando essa
incerteza ao resultado final da solução.
Por fim, temos a \policy que carrega o papel principal de um \rl. A \policy
será readaptada ao longo do aprendizado, baseada na \rewardfunction, em busca
da \policy ótima que, para cada estado, irá decidir pela ação que irá maximizar
a \reward final. Essa \policy existe, porém é muito difícil de ser alcançada,
portanto a busca real é por uma boa aproximação \cite{mahadevan-1996}.

Além dos elementos citados que fazem parte da composição do \rl, temos outro
componente que tem se mostrado cada vez mais necessário e que também é um dos
focos desse trabalho: a analise de resultados e aprimoramento. Através dessa
etapa, é possível descobrir os pontos fortes e fracos da \ia construída,
podendo assim, remodela-la para maximizar os resultados da construção.

Com uma boa construção de cada um destes elementos e da
integração entre eles, o resultado final é uma \ia que consegue se comportar
exatamente como o esperado em um ambiente, sem ao menos ter, inicialmente, sido
ensinada como ele era ou como se comportava. Esse é o grande poder do \rl. 

Tendo essa visão geral do que é o \rl e como ele é composto, vamos nos
aprofundar mais nas técnicas utilizadas para a construção de uma
\rewardfunction e de uma \policy, para então analisar os pontos de interseção
entre as técnicas e as dificuldades em se construir um \framework que
possibilite integrar essas soluções.

\subsection{Função de recompensa}
\label{subsec:text-reward-function}
A \rewardfunction é o componente responsável por dar um valor para cada estado
possível do ambiente. Esse valor representa a \reward que será dada ao agente
ao chegar naquele estado. Se tratando de jogos de tabuleiro, a exemplo do
\Checkers, o número de estados possíveis tende a ser exorbitante, portanto não
é possível definir uma valoração direta, através de uma tabela estado-valor.
Nestes casos, é necessário então o desenvolvimento de uma função aproximada que
será treinada para, em média, dar valores altos para bons estados e baixos para
estados ruins \cite{checkers-2006}.

Para a criação dessa versão aproximada da \rewardfunction existem diversas
técnicas, desde as mais específicas e populadas com bastante informação, até as
mais genéricas que não possuem nenhuma regra associada diretamente ao caso
analisado. As principais técnicas utilizadas são: redes neurais de
multi-camadas, regressão localmente ponderada, árvores de decisão, processo
Gaussiano, abstração de estados e função linear de aproximação
\cite{unifying-rl-2009}. Todas essas técnicas tentam criar funções que, ao
longo de treinamento e da analise dos resultados, irão se aproximar da
\rewardfunction ótima \cite{mahadevan-1996}. Essas técnicas podem ser
categorizadas entre dois tipos: \textit{funções de aproximação lineares} e
\naolineares. As \lineares tem a grande vantagem de serem convergentes (a cada
passo chegam mais perto da função ótima) e terem resultados de forma mais
rápida, porém necessitam de uma boa definição de \features, que são as
características do ambiente (do jogo), para conseguirem alcançar bons
resultados \cite{unifying-rl-2009}. Já as \naolineares, apesar de não serem
convergentes, têm a principal característica de serem mais genéricas,
expressivas e exigirem pouco ou nenhuma informação inicial para desenvolver os
resultados \cite{td-board-games-2005}. Essas vantagens por parte das funções de
aproximação \naolineares, são os principais pontos que têm feito com que esse
tipo de estratégia venha sendo a solução mais utilizada em jogos de tabuleiro.
Por serem bastante complexos, a criação de \features acaba sendo não tão
evidente e desenvolver uma função de aproximação \linear se torna praticamente
impossível. Um exemplo muito usado de função \naolinear para casos complexos
assim são redes neurais.

Para a construção do \framework, teremos um maior aproveitamento de código com
a construção de \rewardfunctions \naolineares uma vez que estas são mais
genéricas e, portanto, mais facilmente aplicadas a vários casos distintos.
Ainda assim , será necessário ter uma preocupação com a possibilidade de
integração de \rewardfunction \lineares, que em casos mais simples e específicos
possuem bons resultados sem muito esforço inicial.

\subsection{Política}
\label{subsec:text-policy}
A \policy é uma função que mapeia um estado e uma ação tomada, a uma
probabilidade, ou seja, a \policy é quem determina as probabilidades de cada
ação ser tomada em um dado estado. Dada a \rewardfunction de um determinado
ambiente, buscar uma \policy que seja ótima é buscar uma \policy que maximize a
recompensa acumulada para qualquer estado inicial existente no espaço estados
possíveis \cite{mitchell-1997}. Apesar de a \policy ótima sempre existir
comprovadamente \cite{bellman-2003}, muitos pontos fazem com que essa busca
seja muito difícil. Os principais pontos são \cite{mitchell-1997}:
\begin{itemize}
  \item Recompensa atrasada: ao contrário da \rewardfunction, onde é somente
  necessário analisar o estado onde se está para determinar um valor, a \policy
  tem que lidar com a questão da \reward a longo prazo. Muitas vezes uma
  sequência de estados com \rewards negativas podem levar o agente a um estado
  de vantagem muito superior a qualquer outro, valendo a pena enfrentar os
  estados ruins.
  \item Exploração: como na grande maioria dos casos, o espaço de estados é
  gigantesco e, portanto, impossível de ser explorado por completo. Desta
  forma, a \policy sempre estará diante do paradoxo \explorationexploitation.
  Em diversos momentos a \policy deve decidir se escolhe o estado conhecido com
  maior recompensa ou um novo estado, ainda não explorado, que pode trazer
  recompensas maiores no futuro.
\end{itemize}

A grande maioria das abordagens utilizadas para tentar encontrar uma \policy
que seja ótima ou que se aproxime desta, se baseiam na ideia da \ValueFunction.

\subsubsection{\ValueFunction}
\label{subsubsec:text-value-function}
A \ValueFunction é uma função que estima o quão bom é para o agente estar num
determinado estado ou tomar uma determinada ação, considerando todas as
possíveis recompensas futuras a partir daquela ação ou estado. Portanto
assumindo uma \policy $\pi$ e um estado \textit{s} $\in$ \textit{S}, o valor
desse estado \textit{s} seguindo a \policy $\pi$, denotado por $V^{\pi}(s)$, é
o valor de recompensa total esperado começando do estado \textit{s} e agindo de
acordo com a \policy $\pi$. Para o caso de utilizar o par estado-ação essa
função é denotada como $Q^{\pi}(s,a)$.
\cite{sutton-barto-1998}

As principais abordagens para construir \policies no \rl são \cite{ghory-2004}:
\begin{itemize}
  \item \DynamicProgramming
  \item \MonteCarlo
  \item \TemporalDifference
\end{itemize}

\subsubsection{\DynamicProgramming}
\label{subsubsec:text-dynamic-programming}
O termo \DynamicProgramming (\dyp) se refere a uma coleção de técnicas
utilizadas para computar a \policy ótima dado um modelo perfeito do ambiente
\cite{sutton-barto-1998} descrito como um \MDP\footnote{O \MDP prover um
\framework matemático para modelagem de de situações de tomada de decisão
envolvendo recompensas associadas. O \textit{MDP} é basicamente composto de um
conjunto finito de estados, um conjunto finito de ações, uma função de
probabilidade para cada ação em um determinado estado e uma função de
recompensa para cada ação \cite{howard-1960}.}. Um ambiente perfeito é aquele
onde o número de estados e ações são finitos e se tem conhecimento de todo o
espaço. As técnicas de \dyp são de utilização muito limitada no campo do \rl,
apesar de serem de extrema importância teórica e terem servido de base para
outras abordagens. Isso acontece, porque na maioria dos problemas de \rl não se
tem um ambiente perfeito. No caso dos jogos de tabuleiro, se tem espaços de
estado e ação finitos, porém eles são grandes o suficiente para se tornarem
computacionalmente desconhecidos. Além disso, as técnicas de \dyp geralmente
tem um gasto computacional muito grande. Esses problemas fizeram surgir
muitas outras abordagens que, basicamente, tentam simular um \dyp, porém com
menor gasto computacional e aproximações dos espaços de estado e ação
\cite{intro-ml-nilsson-1996}.  

A ideia chave do \dyp é usar \valuefunctions para organizar a estrutura de
busca por boas \policies. Baseado nas nas equações de otimalidade de Bellman,
pode-se encontrar \policies ótimas uma vez que encontra-se \valuefunctions
ótimas. Para encontrar \valuefunction ótimas, basta fazer uma conversão das
equações de Bellman em regras de atualização e então ao executar essas regras
até um momento de convergência do resultado, se tem uma \valuefunction ótima,
denotada por $V^*(s)$ ou $Q^*(s,a)$. A equação de Bellman adaptada é a
seguinte:
$$
V^*(s) = \arg\max_a \sum_{s'} P^{a}_{ss'} [R^{a}_{ss'} + \gamma V^*(s')] 
$$
ou
$$
Q^*(s,a) = \sum_{s'} P^{a}_{ss'} [R^{a}_{ss'} + \gamma \arg\max_a Q^*(s',a')] 
$$
onde \textit{a} é a ação tomada, \textit{s} o estado de partida, \textit{s'} o
estado de chegada, $P^a_{ss'}$ a probabilidade de escolher \textit{a} estando
em \textit{s} para partir para \textit{s'}, $R^a_{ss'}$ a recompensa da escolha
de \textit{a} e $\gamma$ é um desconto aplicado. 
\cite{sutton-barto-1998}

Através da avaliação de uma \policy e depois aprimoramento desta e assim
sucessivamente, tende-se a uma convergência à \valuefunction ótima. Os dois
principais algoritmos que implementam esse conceito são: \valueiteration e
\policyiteration.
\cite{sutton-barto-1998}

\subsubsection{\MonteCarlo}
\label{subsubsec:text-monte-carlo}
O \MonteCarlo é um método de aprendizado de estimativa de \valuefunctions e
descoberta de \policies ótimas. Apesar de feito baseado no \dyp, o \mc
tem uma abordagem que não exige um modelo perfeito do ambiente, mas sim,
experiência. O \mc funciona \online, obtendo informações do ambiente ao
passo que toma suas decisões. A grande vantagem de abordagens \online é o fato
delas exigirem pouco ou nenhum conhecimento inicial e ainda assim, conseguirem
produzir \policies ótimas. Assim como no \dyp, o \mc também necessita de
um modelo do ambiente, porém esse modelo não precisa possuir uma completa
distribuição de probabilidades para todas as possíveis transições como no \dyp.
\cite{sutton-barto-1998}

O processo de aprendizado do \mc é feito baseado em episódios. A
experiência do agente é dividida em episódios que tem um começo e um final
definidos e somente ao final de cada episódio a \valuefunction é estimada e
a \policy é aprimorada. Isso garante que se tenha recompensas bem definidas. De
forma resumida, toda a ideia por trás da abordagem de \mc se baseia
em estimar a futura recompensa descontada acumulada esperada para um estado
através da média das estimativas já descobertas nas experiências passadas
naquele estado. Desta forma, quanto mais experiência o agente tem, mas esse
valor irá convergir para o valor esperado real para cada estado.
\cite{sutton-barto-1998}

\subsubsection{\TemporalDifference}
\label{subsubsec:text-temporal-difference}
o \TemporalDifference é uma abordagem de aprendizado construída como uma
combinação do \dyp e do \mc e é considerada uma das abordagens mais centrais do
\rl, principalmente aplicada à jogos de tabuleiro. Assim como o \mc, o \td pode
aprender diretamente das experiências, sem exigir um conhecimento prévio do
ambiente. Como o \dyp, o \td atualiza suas estimativas baseado em outras
estimativas aprendidas sem esperar o resultado final.
\cite{sutton-barto-1998}

O aprendizado do \td não é preso a episódios, mas sim passos. Enquanto o \mc
espera até o final do episódio para realizar a sua atualização, o \td, em sua
implementação mais básica, o \textit{TD(0)}, atualiza as informações
relacionadas ao estado utilizando as informações obtidas ao chegar ao novo
estado. Por ser uma abordagem com bons resultados, o \td teve diversas outras
abordagens que utilizaram essa ideia básica, porém com algumas alterações \cite{sutton-barto-1998}.
Exemplos de outras abordagens são: TD($\lambda$), LSTD, LSTDQ, RTDP, entre
outros \cite{unifying-rl-2009}.

Com o estudo e revisão destas três principais técnicas de construção de
\policies no \rl, percebemos, como previsto, que existe uma grande interseção
entre as técnicas, sendo todas elas baseadas em um centro comum. Essa é uma característica muito interessante para a construção de um \framework. Além disso, existem técnicas, como a \td por exemplo, que possui diversas outras técnicas oriundas dela, porém com pequenas mudanças. Essa é uma outra grande vantagem. Com a construção de algumas técnicas básicas no \framework será possível reutiliza-las para construir outras técnicas que podem ter os resultados mais variados. 
