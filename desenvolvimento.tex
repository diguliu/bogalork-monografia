\chapter{Objetivo geral}

Este trabalho tem como objetivo desenvolver um \framework que sirva de base
para a construção de \ias para jogos de tabuleiro baseadas em \rl. Além disso,
será desenvolvida uma ferramenta para análise comparativa dos resultados que
estará integrada diretamente com o \framework.

\chapter{Objetivos específicos}

\begin{itemize}
  \item Criar o \framework que será composto dos seguintes módulos:
    \begin{itemize}
      \item \textbf{Simulação} - Esse módulo será responsável pela simulação do
      jogo. Ele precisa ser independente de jogo ou técnica de aprendizado.
      Esse módulo deverá permitir a configuração de número de \rounds e
      sessões, assim como possibilitar a ação do modulo de aprendizado em
      qualquer um dos níveis da execução de um jogo, jogada, partida, \round ou
      sessão.
      \item \textbf{Aprendizado} - Esse módulo será responsável pelo
      aprendizado do agente. Ele receberá do simulador informações relativas à
      situação e respostas do jogo para realizar o aprendizado. Ele também terá
      permissão de executar operações a qualquer momento do jogo. Por ser um
      \framework voltado para \rl, esse módulo possuirá o submódulo
      \textbf{Política} que definirá a política de decisão do agente e o
      submódulo \textbf{Função de Recompensa} que definirá a recompensa
      instantânea para um dado estado ou par estado-ação.
      \item \textbf{Jogo} - Esse módulo será responsável pela lógica de
      funcionamento do jogo. Nele estarão definidas elementos como regras de
      transição, ordem de jogada ou número de jogadores.
      \item \textbf{Estado} - Esse módulo definirá o modelo do estado do jogo.
      Apesar desse módulo estar intrinsecamente ligado ao módulo de jogo, ele
      foi propositalmente separado por ser uma das características que tem
      grande influência nos resultados de uma \ia baseada em \rl. Desta forma,
      é possível ter diversas modelagens de estados diferentes para um mesmo
      jogo, sendo possível, posteriormente, compara-las mais facilmente.
      \item \textbf{Agente} - Esse módulo não será um módulo extensível uma vez
      que toda a tomada de decisão do agente vai estar relacionada com o módulo
      de aprendizado. Assim, ele simulará simplesmente a ação de um jogador
      que, na sua vez, toma uma determinada decisão baseada em seu
      conhecimento.
      \item \textbf{Interface} - Esse módulo será o responsável por
      construir a interface gráfica do jogo. Neste trabalho esse módulo não
      será desenvolvido, mas ele estará devidamente conectado aos demais
      módulos permitindo, no futuro, a criação também de interfaces
      modularizadas para os jogos.
    \end{itemize}
  \item Criação de um esquema de banco de dados para o armazenamento tanto dos
  dados do aprendizado quanto dos resultados e comparações feitas
  posteriormente. É necessário também que haja a possibilidade de importação e
  exportação de dados nesse banco, possibilitando, por exemplo, a pré-população
  de um determinado conhecimento já produzido.
  \item Criar uma estrutura de configuração para o jogo, diminuindo, assim, a
  necessidade de desenvolvimento de código para a construção do deste.

  \obs{Existem objetivos que não estão previstos para o trabalho porém podem
  ser desenvolvidos caso dê tempo?}

\end{itemize}

\chapter{Justificativa}

O surgimento da teoria do \ReinforcementLearning é basicamente resultado da
observação do nosso próprio aprendizado em relação ao ambiente
\cite{sutton-barto-1998}. Nós estamos a todo momento realizando ações, buscando
respostas do ambiente, avaliando os resultados e readaptando nosso conhecimento
sobre ele. Desta mesma forma, é aplicada essa ideia para a construção de \ias
que, a partir dos resultados de sua ação em um ambiente, começa a redefinir seu
conhecimento de forma a maximizar seus resultados.

O \ReinforcementLearning já é utilizado como uma técnica de aprendizado de jogos
de tabuleiro desde 1959, quando foi aplicado ao jogo \Checkers
\cite{samuel-1959}. Desde então, o \rl tem se destacado cada vez mais, através
de muitos casos de sucesso, dos demais métodos de aprendizado quando se trata
de jogos de tabuleiro \cite{konen-bartz-2009}. A primeira grande distinção vem
do fato de que a maioria dos jogos de tabuleiro possuem complexidades de
espaço-estado e árvore de possibilidades gigantescas. O xadrez, por exemplo,
tem uma complexidade de espaço-estado de $10^{47}$ e complexidade da árvore de
$10^{123}$ \cite{shannon-1950}. Esses valores são computacionalmente inviáveis
de serem calculados e explorados por completo. Mas por possuir inteligentes
abordagens de aproximação de espaço, o \rl escapa a esse problema de
complexidade conseguindo manter um nível alto de qualidade de resultados. A
segunda grande vantagem é que o \rl pode ser aplicado como um aprendizado
não-supervisionado, descartando a necessidade de um \expert para guiar o agente
no seu aprendizado.

Mas ainda assim, apesar da sua grande evolução, o \rl tem muitos obstáculos
pela frente na sua compreensão e aplicação. Um dos grande obstáculos
encontrados hoje é a falta de resultados comparativos entre as mais diversas
combinações de técnicas (algoritmos, modelos, constantes) e jogos já estudados
\cite{ghory-2004}. Por ser um tema bastante complexo, com tantas abordagens
distintas e composto de pequenas decisões que influenciam bastante o resultado
final do aprendizado, o \rl tem deixadas para trás muitas análises de
variações. A principal carência que gera esse problema é a falta de uma
estrutura que possibilite realizar alterações, obter resultados e gerar
análises comparativas dos elementos de uma \ia facilmente \cite{ghory-2004}.
Além de possibilitar uma analise comparativa, a construção dessa estrutura
traria o benefício do reuso de código para a implementação das mais diversas
\ias, já que, apesar de muito distintas, elas possuem um conjunto de interseção
em sua composição. Um exemplo disso é a realização da simulação do jogo através
de longas sessões de treinamento. Esse conjunto é, a cada nova \ia construída,
reinventado, quando poderia estar sendo reutilizado ou aprimorado.

Visando resolver estes problemas, a solução que se mostrou mais adequada foi a
construção de um \framework, que servirá como base comum para essas \ias. Isso
permitirá o reuso e aprimoramento desse conjunto de interseção entre elas e,
com a integração de uma ferramenta de analise de resultados, possibilitará a
fácil mudança de pequenos pontos para a obtenção de resultados comparativos.
Essa composição, portanto, facilitará a construção de \ias para jogos de
tabuleiro e também servirá para determinar as especificidades de cada técnica e
de cada jogo através da analise de resultados.

\chapter{Revisão da literatura}
\label{cap:conceitos}

Este capítulo abordará os principais conceitos envolvidos na área de \rl e as técnicas aplicadas para alcançar resultados através desse modelo. Também serão abordados os pontos nos quais a teoria acerca desse assunto ainda não foi desenvolvida. Em cada um dos pontos, será analisada a viabilidade e eficiência da solução proposta nesse trabalho.

\section{\ReinforcementLearning}
\label{sec:reinforcementlearning}

O \ReinforcementLearning é um modelo de aprendizado onde o agente, inserido em
um ambiente geralmente desconhecido, realiza ações e, a partir dos resultados
observados no ambiente, readapta sua \policy de escolha de forma a maximizar
suas possibilidades de obter os resultados que ele espera \cite{mitchell-1997}.
Diferentemente do modelo de aprendizado supervisionado onde é "dito"\xspace ao
agente como responder a uma determinada situação, no \ReinforcementLearning o
agente  recebe uma \reward, positiva ou negativa, do ambiente e precisa
aprender, a partir disso, como se comportar no futuro \cite{ghory-2004}. Uma
representação desse modelo pode ser vista na figura \ref{fig:rl-cicle-scheme}.
Apesar de ter do ambiente uma \reward relativa a sua ação, o agente busca,
ainda assim, definir uma \policy que maximize seus resultados a longo prazo e
não somente a curto prazo.

\begin{figure}[h]
  \centering
  \includegraphics[scale=0.5]{imagens/rl-cicle-scheme.png}
  \caption{ciclo de aprendizado no \rl (adaptado de
  \cite{intro-ml-nilsson-1996})}
  \label{fig:rl-cicle-scheme}
\end{figure}

Os principais elementos encontrados em um modelo de \rl são
\cite{intro-ml-nilsson-1996}:
\begin{enumerate}
  \item um conjunto \textit{S} de estados do ambiente;
  \item um conjunto \textit{A} de ações;
  \item uma \rewardfunction \textit{r} para toda ação  $\textit{a} \in$
  \textit{A}; e
  \item uma \policy $\pi$ que define como o agente deve se comportar em um
  estado \textit{S}.
\end{enumerate}

Um dos fatores que torna difícil tecnicamente a aplicação do \rl é que nenhum
dos seus elementos pode ser tratado de forma trivial. Cada um deles possui
características intrínsecas que tornam a composição da solução completa muito
complexa. 
Primeiramente temos a modelagem dos estados do ambiente. Para cada
problema específico temos diversas maneiras de mapeia-los computacionalmente e
a escolha dessa maneira é determinante para o resultado final
\cite{ghory-2004}.
Já o conjunto de ações, possui, na maioria dos problemas, um tamanho inviável
de ser armazenado por completo. Isso limita a solução aos conjuntos que podem
ser representados através de padrões ou fórmulas matemáticas \cite{bouzy}.
Felizmente, no caso dos jogos de tabuleiro, essa é uma característica quase
sempre encontrada.
A \rewardfunction é, na grande maioria dos casos, uma aproximação, uma vez que
está não pode ser uma tabela que mapeie estado-valor dada a limitação espacial.
Portando, esta precisa ser determinada por uma pessoa (um \expert), por uma
função linear, que tende a ser estável e convergente porém pouco expressiva, ou
por um esquema não-linear como redes neurais, que são mais expressivas porém
tendem a não ser estáveis ou convergentes \cite{unifying-rl-2009}. Em qualquer
um dos casos, não se tem um função de \reward evidente, acrescentando essa
incerteza ao resultado final da solução.
Por fim, temos a \policy que carrega o papel principal de um \rl. A \policy
será readaptada ao longo do aprendizado, baseada na \rewardfunction, em busca
da \policy ótima que, para cada estado, irá decidir pela ação que irá maximizar
a \reward final. Essa \policy existe, porém é muito difícil de ser alcançada,
portanto a busca real é por uma boa aproximação \cite{mahadevan-1996}.

Além dos elementos citados que fazem parte da composição do \rl, temos outro
componente que tem se mostrado cada vez mais necessário e que também é um dos
focos desse trabalho: a analise de resultados e aprimoramento. Através dessa
etapa, é possível descobrir os pontos fortes e fracos da \ia construída,
podendo assim, remodela-la para maximizar os resultados da construção.

Com uma boa construção de cada um destes elementos e da
integração entre eles, o resultado final é uma \ia que consegue se comportar
exatamente como o esperado em um ambiente, sem ao menos ter, inicialmente, sido
ensinada como ele era ou como se comportava. Esse é o grande poder do \rl. 

Tendo essa visão geral do que é o \rl e como ele é composto, vamos nos
aprofundar mais nas técnicas utilizadas para a construção de uma
\rewardfunction e de uma \policy, para então analisar os pontos de interseção
entre as técnicas e as dificuldades em se construir um \framework que
possibilite integrar essas soluções.

\subsection{Função de recompensa}
\label{subsec:rewardfunction}
A \rewardfunction é o componente responsável por dar um valor para cada estado
possível do ambiente. Esse valor representa a \reward que será dada ao agente
ao chegar naquele estado. Se tratando de jogos de tabuleiro, a exemplo do
\Checkers, o número de estados possíveis tende a ser exorbitante, portanto não
é possível definir uma valoração direta, através de uma tabela estado-valor.
Nestes casos, é necessário então o desenvolvimento de uma função aproximada que
será treinada para, em média, dar valores altos para bons estados e baixos para
estados ruins \cite{checkers-2006}.

Para a criação dessa versão aproximada da \rewardfunction existem diversas
técnicas, desde as mais específicas e populadas com bastante informação, até as
mais genéricas que não possuem nenhuma regra associada diretamente ao caso
analisado. As principais técnicas utilizadas são: redes neurais de
multi-camadas, regressão localmente ponderada, árvores de decisão, processo
Gaussiano, abstração de estados e função linear de aproximação
\cite{unifying-rl-2009}. Todas essas técnicas tentam criar funções que, ao
longo de treinamento e da analise dos resultados, irão se aproximar da
\rewardfunction ótima \cite{mahadevan-1996}. Essas técnicas podem ser
categorizadas entre dois tipos: \textit{funções de aproximação lineares} e
\naolineares. As \lineares tem a grande vantagem de serem convergentes (a cada
passo chegam mais perto da função ótima) e terem resultados de forma mais
rápida, porém necessitam de uma boa definição de \features, que são as
características do ambiente (do jogo), para conseguirem alcançar bons
resultados \cite{unifying-rl-2009}. Já as \naolineares, apesar de não serem
convergentes, têm a principal característica de serem mais genéricas,
expressivas e exigirem pouco ou nenhuma informação inicial para desenvolver os
resultados \cite{td-board-games-2005}. Essas vantagens por parte das funções de
aproximação \naolineares, são os principais pontos que têm feito com que esse
tipo de estratégia venha sendo a solução mais utilizada em jogos de tabuleiro.
Por serem bastante complexos, a criação de \features acaba sendo não tão
evidente e desenvolver uma função de aproximação \linear se torna praticamente
impossível. Um exemplo muito usado de função \naolinear para casos complexos
assim são redes neurais.

Para a construção do \framework, teremos um maior aproveitamento de código com
a construção de \rewardfunctions \naolineares uma vez que estas são mais
genéricas e, portanto, mais facilmente aplicadas a vários casos distintos.
Ainda assim , será necessário ter uma preocupação com a possibilidade de
integração de \rewardfunction \lineares, que em casos mais simples e específicos
possuem bons resultados sem muito esforço inicial.

\subsection{Política}
\label{subsec:policy}
A \policy é uma função que mapeia um estado e uma ação tomada, a uma
probabilidade, ou seja, a \policy é quem determina as probabilidades de cada
ação ser tomada em um dado estado. Dada a \rewardfunction de um determinado
ambiente, buscar uma \policy que seja ótima é buscar uma \policy que maximize a
recompensa acumulada para qualquer estado inicial existente no espaço estados
possíveis \cite{mitchell-1997}. Apesar de a \policy ótima sempre existir
comprovadamente \cite{bellman-2003}, muitos pontos fazem com que essa busca
seja muito difícil. Os principais pontos são \cite{mitchell-1997}:
\begin{itemize}
  \item Recompensa atrasada: ao contrário da \rewardfunction, onde é somente
  necessário analisar o estado onde se está para determinar um valor, a \policy
  tem que lidar com a questão da \reward a longo prazo. Muitas vezes uma
  sequência de estados com \rewards negativas podem levar o agente a um estado
  de vantagem muito superior a qualquer outro, valendo a pena enfrentar os
  estados ruins.
  \item Exploração: como na grande maioria dos casos, o espaço de estados é
  gigantesco e, portanto, impossível de ser explorado por completo. Desta
  forma, a \policy sempre estará diante do paradoxo \explorationexploitation.
  Em diversos momentos a \policy deve decidir se escolhe o estado conhecido com
  maior recompensa ou um novo estado, ainda não explorado, que pode trazer
  recompensas maiores no futuro.
\end{itemize}

A grande maioria das abordagens utilizadas para tentar encontrar uma \policy
que seja ótima ou que se aproxime desta, se baseiam na ideia da \ValueFunction.

\subsubsection{\ValueFunction}
A \ValueFunction é uma função que estima o quão bom é para o agente estar num
determinado estado ou tomar uma determinada ação, considerando todas as
possíveis recompensas futuras a partir daquela ação ou estado. Portanto
assumindo uma \policy $\pi$ e um estado \textit{s} $\in$ \textit{S}, o valor
desse estado \textit{s} seguindo a \policy $\pi$, denotado por $V^{\pi}(s)$, é
o valor de recompensa total esperado começando do estado \textit{s} e agindo de
acordo com a \policy $\pi$. Para o caso de utilizar o par estado-ação essa
função é denotada como $Q^{\pi}(s,a)$.
\cite{sutton-barto-1998}

As principais abordagens para construir \policies no \rl são \cite{ghory-2004}:
\begin{itemize}
  \item \DynamicProgramming
  \item \MonteCarlo
  \item \TemporalDifference
\end{itemize}

\subsubsection{\DynamicProgramming}
O termo \DynamicProgramming (\dyp) se refere a uma coleção de técnicas
utilizadas para computar a \policy ótima dado um modelo perfeito do ambiente
\cite{sutton-barto-1998} descrito como um \MDP\footnote{O \MDP prover um
\framework matemático para modelagem de de situações de tomada de decisão
envolvendo recompensas associadas. O \textit{MDP} é basicamente composto de um
conjunto finito de estados, um conjunto finito de ações, uma função de
probabilidade para cada ação em um determinado estado e uma função de
recompensa para cada ação \cite{howard-1960}.}. Um ambiente perfeito é aquele
onde o número de estados e ações são finitos e se tem conhecimento de todo o
espaço. As técnicas de \dyp são de utilização muito limitada no campo do \rl,
apesar de serem de extrema importância teórica e terem servido de base para
outras abordagens. Isso acontece, porque na maioria dos problemas de \rl não se
tem um ambiente perfeito. No caso dos jogos de tabuleiro, se tem espaços de
estado e ação finitos, porém eles são grandes o suficiente para se tornarem
computacionalmente desconhecidos. Além disso, as técnicas de \dyp geralmente
tem um gasto computacional muito grande. Esses problemas fizeram surgir
muitas outras abordagens que, basicamente, tentam simular um \dyp, porém com
menor gasto computacional e aproximações dos espaços de estado e ação
\cite{intro-ml-nilsson-1996}.  

A ideia chave do \dyp é usar \valuefunctions para organizar a estrutura de
busca por boas \policies. Baseado nas nas equações de otimalidade de Bellman,
pode-se encontrar \policies ótimas uma vez que encontra-se \valuefunctions
ótimas. Para encontrar \valuefunction ótimas, basta fazer uma conversão das
equações de Bellman em regras de atualização e então ao executar essas regras
até um momento de convergência do resultado, se tem uma \valuefunction ótima,
denotada por $V^*(s)$ ou $Q^*(s,a)$. A equação de Bellman adaptada é a
seguinte:
$$
V^*(s) = \arg\max_a \sum_{s'} P^{a}_{ss'} [R^{a}_{ss'} + \gamma V^*(s')] 
$$
ou
$$
Q^*(s,a) = \sum_{s'} P^{a}_{ss'} [R^{a}_{ss'} + \gamma \arg\max_a Q^*(s',a')] 
$$
onde \textit{a} é a ação tomada, \textit{s} o estado de partida, \textit{s'} o
estado de chegada, $P^a_{ss'}$ a probabilidade de escolher \textit{a} estando
em \textit{s} para partir para \textit{s'}, $R^a_{ss'}$ a recompensa da escolha
de \textit{a} e $\gamma$ é um desconto aplicado. 
\cite{sutton-barto-1998}

Através da avaliação de uma \policy e depois aprimoramento desta e assim
sucessivamente, tende-se a uma convergência à \valuefunction ótima. Os dois
principais algoritmos que implementam esse conceito são: \valueiteration e
\policyiteration.
\cite{sutton-barto-1998}

\subsubsection{\MonteCarlo}
O \MonteCarlo é um método de aprendizado de estimativa de \valuefunctions e
descoberta de \policies ótimas. Apesar de feito baseado no \dyp, o \mc
tem uma abordagem que não exige um modelo perfeito do ambiente, mas sim,
experiência. O \mc funciona \online, obtendo informações do ambiente ao
passo que toma suas decisões. A grande vantagem de abordagens \online é o fato
delas exigirem pouco ou nenhum conhecimento inicial e ainda assim, conseguirem
produzir \policies ótimas. Assim como no \dyp, o \mc também necessita de
um modelo do ambiente, porém esse modelo não precisa possuir uma completa
distribuição de probabilidades para todas as possíveis transições como no \dyp.
\cite{sutton-barto-1998}

O processo de aprendizado do \mc é feito baseado em episódios. A
experiência do agente é dividida em episódios que tem um começo e um final
definidos e somente ao final de cada episódio a \valuefunction é estimada e
a \policy é aprimorada. Isso garante que se tenha recompensas bem definidas. De
forma resumida, toda a ideia por trás da abordagem de \mc se baseia
em estimar a futura recompensa descontada acumulada esperada para um estado
através da média das estimativas já descobertas nas experiências passadas
naquele estado. Desta forma, quanto mais experiência o agente tem, mas esse
valor irá convergir para o valor esperado real para cada estado.
\cite{sutton-barto-1998}

\subsubsection{\TemporalDifference}
o \TemporalDifference é uma abordagem de aprendizado construída como uma
combinação do \dyp e do \mc e é considerada uma das abordagens mais centrais do
\rl, principalmente aplicada à jogos de tabuleiro. Assim como o \mc, o \td pode
aprender diretamente das experiências, sem exigir um conhecimento prévio do
ambiente. Como o \dyp, o \td atualiza suas estimativas baseado em outras
estimativas aprendidas sem esperar o resultado final.
\cite{sutton-barto-1998}

O aprendizado do \td não é preso a episódios, mas sim passos. Enquanto o \mc
espera até o final do episódio para realizar a sua atualização, o \td, em sua
implementação mais básica, o \textit{TD(0)}, atualiza as informações
relacionadas ao estado utilizando as informações obtidas ao chegar ao novo
estado. Por ser uma abordagem com bons resultados, o \td teve diversas outras
abordagens que utilizaram essa ideia básica, porém com algumas alterações \cite{sutton-barto-1998}.
Exemplos de outras abordagens são: TD($\lambda$), LSTD, LSTDQ, RTDP, entre
outros \cite{unifying-rl-2009}.

Com o estudo e revisão destas três principais técnicas de construção de
\policies no \rl, percebemos, como previsto, que existe uma grande interseção
entre as técnicas, sendo todas elas baseadas em um centro comum. Essa é uma característica muito interessante para a construção de um \framework. Além disso, existem técnicas, como a \td por exemplo, que possui diversas outras técnicas oriundas dela, porém com pequenas mudanças. Essa é uma outra grande vantagem. Com a construção de algumas técnicas básicas no \framework será possível reutiliza-las para construir outras técnicas que podem ter os resultados mais variados. 

\section{Questões não analisadas}
\label{sec:questões-não-analisadas}
Apesar da grande evolução do estudo do \rl aplicado a jogos de tabuleiro, boa parte das melhorias e novas técnicas propostas são implementadas em conjunto, de forma dependente, ou somente aplicadas a casos específicos. Esse problema vem acontecendo, principalmente, por conta da falta de infra-estrutura para o reaproveitamento de técnicas já implementadas e pela dificuldade de testar várias combinações de técnicas \cite{ghory-2004}.

Tendo percebido esse problema, o autor Imran Ghory em seu artigo "Reinforcement learning in board games"\xspace realizou uma pesquisa das sugestões dadas pelos mais diversos autores a fim de compilar uma lista com questões que não foram analisadas e as possíveis melhorias que poderiam ser feitas a partir de uma analise comparativa das possibilidades existentes. Abaixo segue a compilação construída por ele:

\obs{Devo colocar essa lista aqui?}

\begin{enumerate}
  \item Possíveis melhorias
  \begin{enumerate}
    \item Melhorias gerais
    \begin{enumerate}
      \item Tamanho das recompensas
      \item Formato do tabuleiro de entrada
      \item O que aprender
      \item Aprendizado repetitivo
      \item Aprendendo de tabuleiros invertidos
      \item Aprendizado em sessões
    \end{enumerate}
    \item Redes neurais
    \begin{enumerate}
      \item Funções não-lineares
      \item Algoritmo de treinamento
    \end{enumerate}
    \item Inicializações randômicas
    \item Técnicas para jogos de \textit{high-branching}
    \begin{enumerate}
      \item Exemplos randômicos
      \item Entrada de decisão parcial
    \end{enumerate}
    \item Melhorias com \textit{self-play}
    \begin{enumerate}
      \item Análises táticas para análises posicionais
      \item Jogador controlando o \textit{self-play}
      \item Escolha do movimento randômica
      \item Avaliação reversa de cor
      \item Avaliação do tabuleiro final informada
    \end{enumerate}
    \item Diferentes algoritmos TD($\lambda$)
    \begin{enumerate}
      \item TD-Leaf
      \item Escolha do valor $\lambda$
      \item Escolha do valor $\alpha$ (taxa de aprendizado)
      \item Algoritmo de coerência temporal
      \item TD-Directed
      \item TD($\mu$)
      \item Parâmetro de decaimento
    \end{enumerate}
  \end{enumerate}
\end{enumerate}

\chapter{Metodologia}
\begin{itemize}
    \item Construir o \framework base para a criação de \ias baseadas no modelo
    de \rl.
    \item Integrar o armazenamento utilizando um banco de dados com a
    possibilidade de importação e exportação de dados.
    \item Construir uma ferramenta integrada ao \framework para a geração e
    analise de resultados comparativos.
    \item Construir uma \ia para o jogo \TicTacToe como exemplo.
\end{itemize}

\chapter{Resultados esperados}

Ao fim deste trabalho, espera-se ter um \framework básico que sirva de base
para a construção de \ias baseadas em \rl, assim como uma ferramenta para
analise de resultados comparativos. Além disso, espera-se ter um exemplo de \ia
implementado sobre esse \framework que sirva de referência para futuras \ias. É
importante ressaltar que o \framework será um \framework bem básico,
desenvolvido de forma a permitir futuras extensões para contemplar as mais
diversas necessidades da construção de \ias baseadas em \rl para jogos de
tabuleiro.

\chapter{Cronograma para conclusão do trabalho}
\begin{center}
  \begin{tabular}{|p{8cm}|c|c|c|c|c|c|}
     \hline
     & Jul & Ago & Set & Out & Nov & Dez \\ 
     \hline
     Construção do \framework                              & x & x & x & x & x & x \\ 
     \hline
     Integração com banco de dados                         &   & x & x &   &   &   \\ 
     \hline
     Construção de ferramenta de análise                   &   &   &   & x &   &   \\ 
     \hline
     Integração da ferramenta de análise com o \framework  &   &   &   &   & x &   \\ 
     \hline
     Implementação do estudo de caso \TicTacToe            &   &   &   &   &   & x \\ 
     \hline
   \end{tabular}
\end{center}
